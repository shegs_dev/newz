from django.shortcuts import render
# from django.http import HttpResponse

# import requests
# from bs4 import BeautifulSoup

from .thread import create_workers, crawl
from .helpers import file_to_set
from .thread import QUEUE_FILE

def index(request):
	create_workers()
	crawl()

	keyword = 'business'
	result = {}
	crawled_data = file_to_set(QUEUE_FILE)
	count = 0
	for link in crawled_data:
		if link.find(keyword) != -1:
			count = count + 1
			result[count] = link

	return render(request, 'posts/index.html', result)
