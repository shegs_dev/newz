import os
from urllib.request import urlopen
from .link_finder import LinkFinder
from .helpers import *

class Crawler:

  # Class variables
  project_name = 'newz_data'
  base_url = 'https://edition.cnn.com'
  domain_name = 'cnn.com'
  queue_file = '/queue.txt'
  crawled_file = '/crawled.txt'
  queue = set()
  crawled = set()
  
  def __init__(self, project_name, base_url, domain_name):
    project_name = Crawler.project_name
    base_url = Crawler.base_url
    domain_name = Crawler.domain_name
    Crawler.queue_file = os.getcwd() + '/' + Crawler.project_name + '/queue.txt'
    Crawler.crawled_file = os.getcwd() + '/' + Crawler.project_name + '/crawled.txt'
    self.boot()
    self.crawl_page('Attempting initial scraping:', Crawler.base_url)
    
  @staticmethod
  def boot():
    create_project_dir(Crawler.project_name)
    create_data_files(Crawler.project_name, Crawler.base_url)
    Crawler.queue = file_to_set(Crawler.queue_file)
    Crawler.crawled = file_to_set(Crawler.crawled_file)
    
  @staticmethod
  def crawl_page(thread_name, page_url):
    if len(Crawler.queue) > 1500:
      return
    if page_url not in Crawler.crawled:
      print(thread_name + ' now crawling ' + page_url)
      print('Queue ' + str(len(Crawler.queue)) + ' | crawled ' + str(len(Crawler.crawled)))
      Crawler.add_links_to_queue(Crawler.gather_links(page_url))
      Crawler.queue.remove(page_url)
      Crawler.crawled.add(page_url)
      Crawler.update_files()
      
  @staticmethod
  def gather_links(page_url):
    html_string = ''
    try:
      response = urlopen(page_url)
      if dict(urlopen(page_url).info()).get("Content-Type", "none").split('; ')[0] == 'text/html':
        html_bytes = response.read()
        html_string = html_bytes.decode("utf-8")
      finder = LinkFinder(Crawler.base_url, page_url)
      finder.feed(html_string)
    except:
      print('Error: can not crawl page')
      return set()
    return finder.page_links()
    
  @staticmethod
  def add_links_to_queue(links):
    for url in links:
      if url in Crawler.queue:
        continue
      if url in Crawler.crawled:
        continue
      if Crawler.domain_name not in url:
        continue
      Crawler.queue.add(url)
      
  @staticmethod
  def update_files():
    set_to_file(Crawler.queue, Crawler.queue_file)
    set_to_file(Crawler.crawled, Crawler.crawled_file)

